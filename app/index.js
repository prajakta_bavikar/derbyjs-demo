var derby = require('derby');
var app = module.exports = derby.createApp('studentsApp', __filename);

global.app = app;

app.loadViews (__dirname+'/views');
app.loadStyles(__dirname+'/css');

app.on('model', function(model) {
 model.fn('all',       function(item) { return true; });
   model.fn('submitted', function(item) { return  item.submitted;}); 
  model.fn('yetToSubmit',    function(item) { return !item.submitted;});
 
  model.fn('counters', function(students){

    var counters = { submitted: 0, yetToSubmit: 0 };

    for (var id in students) {
      if(students[id].submitted) counters.submitted++; else counters.yetToSubmit++;
    }
    return counters;
  })

});

app.get('/',          getPage('all'));
app.get('/yetToSubmit',    getPage('yetToSubmit')); 
app.get('/submitted', getPage('submitted'));

function getPage(filter){
  return function(page, model) {
    model.subscribe('students', function () {
      model.filter('students', filter).ref('_page.students');
      model.start('_page.counters', 'students', 'counters');
      page.render();
    });
  }
}

app.proto.addStudent = function(newStudent){

  if (!newStudent) return;

  this.model.add('students', {
    fname: newStudent,    
    submitted: false
  });

  this.model.set('_page.newStudent', '');
};

app.proto.delStudent = function(studentId){
  this.model.del('students.' + studentId);
};

app.proto.clearSubmitted = function(){
  var students = this.model.get('students');

  for (var id in students) {
    if (students[id].submitted) this.model.del('students.'+id);
  }
}

app.proto.editStudent = function(student){

  this.model.set('_page.edit', {
    id: student.id,
    fname: student.fname
  });
  
  window.getSelection().removeAllRanges();
  document.getElementById(student.id).focus()
}

app.proto.doneEditing = function(student){
  this.model.set('students.'+student.id+'.fname', student.fname);
  this.model.set('_page.edit', {
    id: undefined,
    fname: ''
  });
}

app.proto.cancelEditing = function(e){
  // 27 = ESQ-key
  if (e.keyCode == 27) {
    this.model.set('_page.edit.id', undefined);
  }
}
